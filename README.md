# SE Academy Compfest

Cara menjalankan Program GO-EAT
1. buka cmd lalu pergi ke directory tempat anda menyimpan program ini
2. lalu jalankan program dengan memilih 1 dari 3 cara ini
    2.1 menjalankan program dengan ruby go-eat.rb
    2.2 menjalankan program dengan ruby go-eat.rb int int int
        int dapat diubah dengan angka yang anda inginkan
        int yg pertama merupakan besar maps yang diinginkan
        int yg kedua dan ketiga merupakan posisi user dengan format x 	dan y
    2.3 menjalankan program dengan ruby go-eat.rb namafile.txt
    jika terdapat kesalahan pada argumen seperti file tidak ada maka program TIDAK DAPAT DIJALANKAN
3. Program initiate
    2.1 jika anda menjalankan program dengan cara yang a maka maps,posisi User, posisi menu dan Harga pada Store,
        serta posisi driver dan jumlah driver akan ditentukan oleh sistem.
        Dan posisi akan di random oleh Sistem setelah itu anda dapat lanjut ke step nomor 5   
    2.2 jika anda menjalankan program dengan cara yang b maka anda akan diminta melakukan hal pada nomor 4.
    2.3 jika anda menjalankan program dengan cara yang c* maka langsung berjalan ke step nomor 5
        *Di dalam file harus sudah ditentukan store,user,driver serta maps untuk program tersebut keterangan file akan dijelaskan lebih lanjut dibawah
4. Step untuk meng-generate Store dan Driver
    4.1 program akan meminta anda untuk memasukan 5 nama driver
    4.2 setelah itu anda akan diminta untuk memasukkan 3 nama store
	4.3 pada setiap store anda akan diminta memasukan jumlah menu
	4.4 pada setiap menu anda akan diminta memasukan nama dan harga pada menu tersebut
5. Perintah yang dapat dijalankan pada program ini ada 4:
    5.1 Show Maps perintah ini digunakan untuk memperlihatkan maps pada Program
    5.2 Order perintah ini digunakan jika ingin melakukan sebuah order pada program tersebut
    5.3 History perintah ini digunakan untuk memperlihatkan order yang telah dilakukan jika belum ada order yang dilakukan maka tidak memperlihatkan apa pun
    5.4 exit perintah ini dapat digunakan untuk keluar dari program
6. Proses Order
    6.1 User akan diminta untuk memilih Restoran yg diinginkan
    6.2 Setelah itu User akan memilih menu
    6.3 Setelah itu User akan memesan jumlah yang diinginkan
    6.4 setelah itu User akan ditanya apakah ingin menambah menu atau tidak
    6.5 jika ingin menambah menu maka proses akan berulang pada 6.2
    6.6 jika tidak maka Akan muncul Menu yang telah di order oleh User serta jumlah dan total harga
    6.7 User akan diminta untuk merating driver setelah makanan sampai
    6.8 proses order selesai maka akan diberikan file bertipe txt yg berisikan nama driver,route yang diambil, nama store, item yang dibeli, dan harga. mengapa yg dipilih tipe txt karena file tersebut paling mudah dibuat

7. Penjelasan File input.txt
    7.1 