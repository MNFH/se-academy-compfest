class Driver
    def initialize(nama = "hehe", rating = 0.0, posisiX = 0, posisiY = 0, orderTaken = 0)
        @nama = nama
        @rating = rating
        @posisiX = posisiX
        @posisiY = posisiY
        @orderTaken = orderTaken
    end
    #getter
    def nama
        @nama
    end

    def rating
        @rating
    end

    def posisiX
        @posisiX
    end

    def posisiY
        @posisiY
    end
    
    def orderTaken
        @orderTaken
    end
    #getter
    def posisiX=(newPosition)
        @posisiX = newPosition
    end
    
    def posisiY=(newPosition)
        @posisiY = newPosition
    end
    
    def rating=(ratings)
        @rating = avg(ratings)
    end

    def orderTaken=(newOrder)
        @orderTaken = @orderTaken + newOrder
    end

    # methon untuk membuat rata rata ratings
    def avg(ratings)
        @rating = (@rating*@orderTaken + ratings)/(@orderTaken+1)
    end
end

class Store
    #initialize the Class
    def initialize(nama = "Rumah Makan Padang", menu = ["Rendang", "Ayam Bakar","Telor Balado"],
        harga = {"Rendang" => 12000, "Ayam Bakar" => 10000, "Telor Balado" => 8000}, posisiX = 0, posisiY = 0)
        @nama = nama
        @menu = menu
        @harga = harga
        @posisiX = posisiX
        @posisiY = posisiY
    end
   
    #getter
    def nama
        @nama
    end
    
    def menu
        @menu
    end
    
    def harga
        @harga
    end

    def posisiX
        @posisiX
    end
    
    def posisiY
        @posisiY
    end

    #setter
    def posisiX=(newPosition)
        @posisiX = newPosition
    end
    
    def posisiY=(newPosition)
        @posisiY = newPosition
    end
end

class History
    def initialize(store,menu,jumlahTotal,totalHarga)
        @store = store
        @menu = menu
        @jumlahTotal = jumlahTotal
        @totalHarga = totalHarga
    end

    def store
        @store
    end
    
    def menu
        @menu
    end
    
    def jumlahTotal
        @jumlahTotal
    end

    def totalHarga
        @totalHarga
    end
end

#method untuk membuat store dan driver secara default dari program
def random(driver, stores, maps)
    driver.push(Driver.new("Bambang"))
    driver.push(Driver.new("Joko"))
    driver.push(Driver.new("Soeratno"))
    driver.push(Driver.new("Rizal"))
    driver.push(Driver.new("Ismail"))
    stores.push(Store.new)
    stores.push(Store.new("Kedai Minum",["Green Tea","Thai Tea","Teh Oolong","Taro","Oreo"],
        {"Green Tea" => 12000, "Thai Tea" => 12000, "Teh Oolong" => 7000, "Taro" => 10000, "Oreo" => 15000}))
    stores.push(Store.new("Ayam Enak",["Ayam goreng","Ayam Bakar","Nasi","Opor Ayam","Sop Ayam","Ayak Geprek Maknyos"],
        {"Ayam goreng" => 15000, "Ayam Bakar" => 17000, "Nasi" => 4000, "Opor Ayam" => 15000, "Sop Ayam" => 20000, "Ayak Geprek Maknyos" => 17000}))
    
    #memasukan seluruh driver dan store ke dalam maps
    driver.each do |driver|
        findPosition = true
        while findPosition do
            posisiX = rand(maps.length)
            posisiY = rand(maps.length)
            if(maps[posisiX][posisiY] == ".")
                driver.posisiX = posisiX
                driver.posisiY = posisiY
                findPosition = false
                maps[posisiX][posisiY] = driver.nama.chr
            end
        end
    end

    stores.each do |store|
        findPosition = true
        while findPosition do
            posisiX = rand(maps.length)
            posisiY = rand(maps.length)
            if(maps[posisiX][posisiY] == ".")
                store.posisiX = posisiX
                store.posisiY = posisiY
                findPosition = false
                maps[posisiX][posisiY] = stores.index(store)+1
            end
        end
    end
    
end

# method untuk membuat store dan driver sesuai dengan input dari user
def generateMaps(drivers,stores,maps)
    puts "silahkan masukkan 5 nama driver yang diinginkan"
    i = 0
    while i < 5 do
        driver = gets.chomp
        drivers.push(Driver.new(driver))
        i = i + 1
    end
    puts "silahkan masukkan 3 stores yang anda inginkan"
    i = 0
    while i < 1 do
        puts "Store ke #{i+1}"
        store = gets.chomp
        puts "brp menu yang ada pada #{store}"
        jumlahMenu = gets.to_i
        j = 0
        menu = []
        harga = {}
        puts "apa nama menunya"
        while j < jumlahMenu do
            menu.push(gets.chomp)
            puts "berapa harganya?"
            harga[menu[j]] = gets.to_i
            j = j + 1
        end
        stores.push(Store.new(store,menu,harga))
        i = i + 1
    end

     #memasukan seluruh driver dan store ke dalam maps
     drivers.each do |driver|
        findPosition = true
        while findPosition do
            posisiX = rand(maps.length)
            posisiY = rand(maps.length)
            if(maps[posisiX][posisiY] == ".")
                driver.posisiX = posisiX
                driver.posisiY = posisiY
                findPosition = false
                maps[posisiX][posisiY] = driver.nama.chr
            end
        end
    end

    stores.each do |store|
        findPosition = true
        while findPosition do
            posisiX = rand(maps.length)
            posisiY = rand(maps.length)
            if(maps[posisiX][posisiY] == ".")
                store.posisiX = posisiX
                store.posisiY = posisiY
                findPosition = false
                maps[posisiX][posisiY] = stores.index(store)+1
            end
        end
    end
end

# method untuk melihat maps
# * untuk User
# alpabet untuk driver
# angka untuk stores
def showMaps(maps)
    i = 0
    while i < maps.length do
        k = 0
        while k < maps[i].length do
            print maps[i][k]
            k = k + 1
        end
        i = i + 1
        puts
    end
end

#melakukan order
def order(stores,drivers,maps,posisiX,posisiY,histories)
    puts "Silahkan order yang anda inginkan"
    puts "pilih store yang anda inginkan (1,2,3,..)"
    stores.each do |store|
        puts "#{stores.index(store)+1}. #{store.nama}"
    end
    pilihan = gets.to_i - 1
    order = true
    orderan = Array.new
    
    puts "berikut adalah menu pada #{stores[pilihan].nama}:"
    stores[pilihan].menu.each do |menu|
        puts "#{stores[pilihan].menu.index(menu)+1}. #{menu} #{stores[pilihan].harga[menu]}"
    end

    puts "Apa yang ingin anda pesan?"
    pesanan = []
    jumlahTotal = {}
    while order do
        pesan = gets.chomp
        if (pesan.to_i > 0 && pesan.to_i <= stores[pilihan].menu.length) || stores[pilihan].menu.index(pesan) != nil
            puts "Berapa jumlah yang ingin anda pesan?"
            jumlah = gets.to_i
            while jumlah < 1 do
                puts "Berapa jumlah yang ingin anda pesan?"
                jumlah = gets.to_i
            end

            if pesan.to_i > 0
                pesanan.push(stores[pilihan].menu[pesan.to_i-1])
            else
                pesanan.push(pesan)
            end
            jumlahTotal[pesanan[pesanan.length - 1]] = jumlah

            puts "Apakah anda ingin menambah pesanan"
            puts "Ya/Tidak"
            tambah  = gets.chomp
            if tambah == "Ya"
                puts "Menu apa yang ingin anda tambah?"
            else
                order = false
            end

        else
            puts "tidak ada menu tersebut"
        end
    end
    puts "Ini adalah pesanan anda"
    puts "    Makanan    Jumlah"
    pesanan.each do |pesan|
        puts "#{pesanan.index(pesan)+1}. #{pesan.ljust(15)}#{jumlahTotal[pesan]}"
    end
    jarak = storeToUser(stores[pilihan],posisiX,posisiY)
    total = totalHarga(stores[pilihan],pesanan,jumlahTotal,jarak)
    puts "Total Harga #{total}"
    driver = findDriver(drivers,stores[pilihan])
    JalanDriver(stores[pilihan],driver,posisiX,posisiY)
    puts
    puts "Selamat menikmati hidangan silahkan rating driver kami"
    rating = gets.to_i
    driver.rating = rating
    driver.orderTaken = 1
    #pengecekan rating driver jika kurang dari 3.0 maka akan dihapus dari 
    if driver.rating < 3.0
        maps[driver.posisiX][driver.posisiY] = "."
        drivers.delete(driver)
    end

    # menambah history
    histories.push(History.new(stores[pilihan],pesanan,jumlahTotal,total))

    #ini buat bikin file baru yaa
end

# method untuk melihat History pembelian
def showHistory(histories)
    histories.each do |history|
        puts "#{histories.index(history)+1}. #{history.store.nama}"
        history.menu.each do |menu|
            puts "  #{menu}   #{history.jumlahTotal[menu]}"
        end
        puts " total #{history.totalHarga}"
    end
end

def jarakDriver(driver, store)
    hasil = 0
    tempX = store.posisiX - driver.posisiX
    if(tempX < 0)
        tempX = tempX * -1
    end
    tempY = store.posisiY - driver.posisiY
    if(tempY < 0)
        tempY = tempY * -1
    end
    hasil = tempX + tempY
    return hasil
end

def findDriver(drivers,store)
    # jika driver tidak ada maka membuat driver baru
    if drivers.length == 0
        puts "Looking for driver"
        drivers.push(Driver.new("Adi"))
        drivers.push(Driver.new("Syamsul"))
        drivers.push(Driver.new("Toriq"))
         #memasukan seluruh driver dan store ke dalam maps
        drivers.each do |driver|
            findPosition = true
            while findPosition do
                posisiX = rand(maps.length)
                posisiY = rand(maps.length)
                if(maps[posisiX][posisiY] == ".")
                    driver.posisiX = posisiX
                    driver.posisiY = posisiY
                    findPosition = false
                    maps[posisiX][posisiY] = driver.nama.chr
                end
            end
        end
    end
    min = jarakDriver(drivers[0],store)
    hasil = drivers[0]
    drivers.each do |driver|
        if jarakDriver(driver,store) < min
            max = jarakDriver(driver,store)
            hasil = driver
        end   
    end
    return hasil
end
# jalan driver
def JalanDriver(store,driver,posisiX,posisiY)
    puts "driver is on the way to store, start at (#{driver.posisiX},#{driver.posisiY})"
    dXposition = driver.posisiX
    dYposition = driver.posisiY
    sXposition = store.posisiX
    sYposition = store.posisiY
    while dXposition != sXposition do
        if dXposition > sXposition
            dXposition = dXposition - 1
        else
            dXposition = dXposition + 1
        end
        if dXposition == sXposition && dYposition == sYposition
            puts "go to (#{dXposition},#{dYposition}), driver arrived at the store!"
        else
            puts "go to (#{dXposition},#{dYposition})"
        end

    end
    while dYposition != sYposition do
        if dYposition > sYposition
            dYposition = dYposition - 1
        else
            dYposition = dYposition + 1
        end
        if dXposition == sXposition && dYposition == sYposition
            puts "go to (#{dXposition},#{dYposition}), driver arrived at the store!"
        else
            puts "go to (#{dXposition},#{dYposition})"
        end    
    end
    while dXposition != posisiX do
        if dXposition > posisiX
            dXposition = dXposition - 1
        else
            dXposition = dXposition + 1
        end
        if dXposition == posisiX && dYposition == posisiY
            puts "go to (#{dXposition},#{dYposition}), driver arrived at your place!"
        else
            puts "go to (#{dXposition},#{dYposition})"
        end 
    end
    while dYposition != posisiY do
        if dYposition > posisiY
            dYposition = dYposition - 1
        else
            dYposition = dYposition + 1
        end        
        if dXposition == posisiX && dYposition == posisiY
            puts "go to (#{dXposition},#{dYposition}), driver arrived at your place!"
        else
            puts "go to (#{dXposition},#{dYposition})"
        end 
    end
end

# menghitung jarak dari store ke user
def storeToUser(stores,posisiX,posisiY)
    hasil = 0
    tempX = stores.posisiX - posisiX
    if(tempX < 0)
        tempX = tempX * -1
    end
    tempY = stores.posisiY - posisiY
    if(tempY < 0)
        tempY = tempY * -1
    end
    hasil = tempX + tempY
    return hasil
end

def totalHarga(stores, menus, jumlah, jarak)
    hasil = 0
    menus.each do |menu|
        hasil = hasil + stores.harga[menu] * jumlah[menu]
    end
    return hasil + jarak*250 #harga per petak adalah 250
end


#global variable
maps = []
stores = []
drivers = []
histories = []

begin
    file = ARGV[0]
    File.open(file,"r").each do |line|
        puts line
    end

rescue => exception 
    i = 1
    jumlahMaps = 20
    posisiX = -1
    posisiY = -1
    ARGV.each do |line|
        if i == 1
            jumlahMaps = line.to_i
            if jumlahMaps == 0
                puts "file not exist"
                exit(true)
            end
        elsif i == 2
            posisiX = line.to_i
            if posisiX > jumlahMaps
                puts "posisi melebihi kapasitas maps"
                exit(true)
            end
        else
            posisiY = line.to_i
            if posisiY > jumlahMaps
                puts "posisi melebihi kapasitas maps"
                exit(true)
            end
        end
        i = i + 1
    end
    #membuat peta dasar
    ARGV.clear #menghapus ARGV
    maps = Array.new(jumlahMaps) {Array.new(jumlahMaps) {"."}}
    if posisiX < 0 or posisiY < 0
        findPosition = true
        while findPosition do
            posisiX = rand(maps.length)
            posisiY = rand(maps.length)
            if(maps[posisiX][posisiY] == ".")
                maps[posisiX][posisiY] = "*"
                findPosition = false
            end
        end
        random(drivers,stores, maps)
    else 
        maps[posisiX][posisiY] = "*"
        generateMaps(drivers,stores,maps)
    end
else
    #no exception
end
game = true
puts "Masukkan perintah Show Maps untuk melihat maps yang ada"
puts "Masukkan perintah Order untuk memulai order"
puts "Masukkan perintah History untuk melihat History pembelian"
puts "Masukkan perintah exit jika ingin keluar dari program"
while game do
    command = gets.chomp
    if command == "Show Maps"
        showMaps(maps)
    elsif command == "Order"
        order(stores,drivers,maps,posisiX,posisiY,histories)
    elsif command == "History"
        showHistory(histories)
    elsif command == "exit"
        game = false
    else
        puts "perintah salah" 
    end  
end

